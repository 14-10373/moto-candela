// MONGO SETUP
import { MongoClient, ObjectId } from 'mongodb';
import fs from 'fs';
import dotenv from 'dotenv';
dotenv.config();

const credentials = process.env.MONGO_CERTIFICATE_PATH
const mongoURL = process.env.MONGO_URL;

const client = new MongoClient(mongoURL, {
  tls: true,
  tlsCertificateKeyFile: credentials,
  tlsAllowInvalidCertificates: true
});

async function run() {
  try {
    await client.connect();
    const db = client.db(process.env.MONGO_DB);
    const Motorcycle = db.collection('Motorcycle');
    const Replacement = db.collection('Replacement');
    const Category = db.collection('Category');
    const User = db.collection('User');
    const Order = db.collection('Order');


    // Delete all documents
    await Motorcycle.deleteMany({});
    await Replacement.deleteMany({});
    await Category.deleteMany({});
    await User.deleteMany({});
    await Order.deleteMany({});
    await db.collection('counters').deleteMany({});
    console.log('Deleted all documents');

    let data = fs.readFileSync('moto-candela.motorcycles.json', 'utf8');
    const motorcycles = JSON.parse(data);
    motorcycles.forEach(motorcycle => {
      if (motorcycle._id?.$oid) {
        motorcycle._id = new ObjectId(motorcycle._id.$oid);
      }
    });
    await Motorcycle.insertMany(motorcycles);

    data = fs.readFileSync('moto-candela.replacements.json', 'utf8');
    const replacements = JSON.parse(data);
    replacements.forEach(replacement => {
      if (replacement._id?.$oid) {
        replacement._id = new ObjectId(replacement._id.$oid);
      }
    });
    await Replacement.insertMany(replacements);

    data = fs.readFileSync('moto-candela.categories.json', 'utf8');
    const categories = JSON.parse(data);
    categories.forEach(category => {
      if (category._id?.$oid) {
        category._id = new ObjectId(category._id.$oid);
      }
    });
    await Category.insertMany(categories);

    await db.collection('counters').insertMany([
      { _id: "motorcycles", sequence_value: motorcycles.length },
      { _id: "replacements", sequence_value: replacements.length },
      { _id: "categories", sequence_value: categories.length },
      { _id: "users", sequence_value: 0},
      { _id: "orders", sequence_value: 0}
    ]);

    await Motorcycle.createIndex({ "id": 1 }, { unique: true });
    await Replacement.createIndex({ "id": 1 }, { unique: true });
    await Category.createIndex({ "id": 1 }, { unique: true });

    await User.createIndex({ "email": 1 }, { unique: true });
    await User.createIndex({ "id": 1 }, { unique: true });

    await Order.createIndex({ "id": 1 }, { unique: true });


    console.log('Created all documents');
    console.log('Done!');

  } catch (err) {
    console.log(err.stack);
  } finally {
    // Ensures that the client will close when you finish/error
    await client.close();
  }
}

run().catch(console.dir);