import { MongoClient } from "mongodb";
import express from "express";
import cors from "cors";
import jwt from "jsonwebtoken";
import bcrypt from "bcrypt";
import dotenv from "dotenv";
import cookieParser from "cookie-parser";
dotenv.config();

const saltRounds = 10;

const credentials = process.env.MONGO_CERTIFICATE_PATH;
const mongoURL = process.env.MONGO_URL;

const client = new MongoClient(mongoURL, {
	tls: true,
	tlsCertificateKeyFile: credentials,
	tlsAllowInvalidCertificates: true,
});

const html = `
<!DOCTYPE html>
<html>
  <head>
    <title>Hello from Moto Candela Service</title>
    <script src="https://cdn.jsdelivr.net/npm/canvas-confetti@1.5.1/dist/confetti.browser.min.js"></script>
    <script>
      setTimeout(() => {
        confetti({
          particleCount: 100,
          spread: 70,
          origin: { y: 0.6 },
          disableForReducedMotion: true
        });
      }, 500);
    </script>
    <style>
      @import url("https://p.typekit.net/p.css?s=1&k=vnd5zic&ht=tk&f=39475.39476.39477.39478.39479.39480.39481.39482&a=18673890&app=typekit&e=css");
      @font-face {
        font-family: "neo-sans";
        src: url("https://use.typekit.net/af/00ac0a/00000000000000003b9b2033/27/l?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n7&v=3") format("woff2"), url("https://use.typekit.net/af/00ac0a/00000000000000003b9b2033/27/d?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n7&v=3") format("woff"), url("https://use.typekit.net/af/00ac0a/00000000000000003b9b2033/27/a?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n7&v=3") format("opentype");
        font-style: normal;
        font-weight: 700;
      }
      html {
        font-family: neo-sans;
        font-weight: 700;
        font-size: calc(62rem / 16);
      }
      body {
        background: white;
      }
      section {
        border-radius: 1em;
        padding: 1em;
        position: absolute;
        top: 50%;
        left: 50%;
        margin-right: -50%;
        transform: translate(-50%, -50%);
      }
    </style>
  </head>
  <body>
    <section>
      Hello from Moto Candela Service!
    </section>
  </body>
</html>
`;

const app = express();
const port = process.env.PORT || 3001;
app.disable('x-powered-by');

app.get("/", (req, res) => res.type("html").send(html));

app.use(
	cors({
		origin: process.env.CLIENT_URL,
		credentials: true,
	})
);

app.use(async (req, res, next) => {
	try {
		await client.connect();
		req.db = client.db(process.env.MONGO_DB);
		req.Motorcycle = req.db.collection("Motorcycle");
		req.Replacement = req.db.collection("Replacement");
		req.Category = req.db.collection("Category");
		req.Order = req.db.collection("Order");
		req.User = req.db.collection("User");
		req.counters = req.db.collection("counters");
		next();
	} catch (err) {
		console.error(err);
		res.status(500).json({
			error: "An error occurred while connecting to the database",
		});
	}
});

// GET endpoints
app.get("/api/rest/v1/motorcycles/:id", async (req, res) => {
	const motorcycle = await req.Motorcycle.findOne({
		id: parseInt(req.params.id),
	});
	res.json(motorcycle);
});

app.get("/api/rest/v1/motorcycles", async (req, res) => {
	const motorcycles = await req.Motorcycle.find().toArray();
	res.json(motorcycles);
});

app.get("/api/rest/v1/replacements/:id", async (req, res) => {
	const replacement = await req.Replacement.findOne({
		id: parseInt(req.params.id),
	});
	res.json(replacement);
});

app.get("/api/rest/v1/replacements", async (req, res) => {
	const replacements = await req.Replacement.find().toArray();
	res.json(replacements);
});

app.get("/api/rest/v1/categories/:id", async (req, res) => {
	const category = await req.Category.findOne({
		id: parseInt(req.params.id),
	});
	res.json(category);
});

app.get("/api/rest/v1/categories", async (req, res) => {
	const categories = await req.Category.find().toArray();
	res.json(categories);
});

app.get("/api/rest/v1/orders/:id", async (req, res) => {
	const order = await req.Order.findOne({
		id: parseInt(req.params.id),
	});
	res.json(order);
});

app.get("/api/rest/v1/orders", async (req, res) => {
	const orders = await req.Order.find().toArray();
	res.json(orders);
});

app.get("/api/rest/v1/users/:id", async (req, res) => {
	const user = await req.User.findOne({
		id: parseInt(req.params.id),
	});
	res.json(user);
});

app.get("/api/rest/v1/users", async (req, res) => {
	const users = await req.User.find().toArray();
	res.json(users);
});

app.use(cookieParser());

app.get("/api/rest/v1/authenticateUser", async (req, res) => {
	const token = req.cookies.token;

	if (!token) {
		return res.json(null);
	}

	try {
		const payload = jwt.verify(token, process.env.JWT_SECRET);
		const user = await req.User.findOne({ id: payload.id });

		delete user.password;
		
		return res.json(user);
	} catch (err) {
		return res.json(null);
	}
});

// This line is needed to parse JSON request body
app.use(express.json());

async function getNextSequenceValue(req, sequenceName) {
	const sequenceDocument = await req.counters.findOneAndUpdate(
		{ _id: sequenceName },
		{ $inc: { sequence_value: 1 } },
		{ returnDocument: "after" }
	);
	return sequenceDocument.value.sequence_value;
}

// POST endpoints follow the same pattern
const insertProduct = async (req, res, collection, counterName) => {
	req.body.id = await getNextSequenceValue(req, counterName);
	try {
		const result = await collection.insertOne(req.body);
		result.id = req.body.id;
		res.json(result);
	} catch (err) {
		res.status(500).json({
			message: `Insert operation failed for ${counterName}`,
			error: err.message,
		});
	}
};

app.post("/api/rest/v1/motorcycles", async (req, res) => {
	await insertProduct(req, res, req.Motorcycle, "motorcycles");
});

app.post("/api/rest/v1/replacements", async (req, res) => {
	await insertProduct(req, res, req.Replacement, "replacements");
});

app.post("/api/rest/v1/categories", async (req, res) => {
	await insertProduct(req, res, req.Category, "categories");
});

app.post("/api/rest/v1/orders", async (req, res) => {
	// DELETE: esto es para subir una copia "compatible" en la coleccion "Ordenes"
	const Ordenes = req.db.collection("Ordenes");
	// Formato de la orden en la coleccion "Ordenes"
	const orden = {
		"Fecha": req.body.date,
		"Usuario": req.body.user.email,
		"Productos": JSON.stringify(req.body.items),
		"Monto": req.body.amount.$numberDecimal,
		"Metodo de pago": req.body.paymentMethod,
		"Cambio": req.body.exchangeRate.$numberDecimal,
		"Estado": req.body.status,
	};

	try {
		await Ordenes.insertOne(orden);
	} catch (err) {
		res.status(500).json({
			message: `Insert operation failed for ordenes`,
			error: err.message,
		});
	}

	// Insert original
	await insertProduct(req, res, req.Order, "orders");
});

app.post("/api/rest/v1/users", async (req, res) => {
	req.body.id = await getNextSequenceValue(req, "users");
	try {
		const hashedPassword = await bcrypt.hash(req.body.password, saltRounds);
		req.body.password = hashedPassword;
		const result = await req.User.insertOne(req.body);
		result.id = req.body.id;
		res.status(200).json(result);
	} catch (err) {
		res.status(500).json({
			message: "Insert operation failed",
			error: err.message,
		});
	}
});

app.post("/api/rest/v1/users/login", async (req, res) => {
	try {
		const user = await req.User.findOne({
			email: req.body.email,
		});
		if (!user) {
			res.status(404).json({
				message: "Login failed",
				error: "Usuario inexistente",
			});
			return;
		}
		if (!(await bcrypt.compare(req.body.password, user.password))) {
			res.status(401).json({
				message: "Login failed",
				error: "Contraseña incorrecta",
			});
			return;
		}

		// Generate JWT token
		const token = jwt.sign({ id: user.id }, process.env.JWT_SECRET, {
			expiresIn: "30d",
		});

		res.cookie("token", token, {
			httpOnly: true,
			secure: false,
			sameSite: "Lax",
			maxAge: 30 * 24 * 60 * 60 * 1000,
		}); // Cookie expires after 30 days

		delete user.password;
		res.status(200).json(user);
	} catch (err) {
		res.status(500).json({ message: "Login failed", error: err.message });
	}
});

app.post("/api/rest/v1/users/logout", async (req, res) => {
	res.clearCookie("token", {
		httpOnly: true,
		secure: false,
		sameSite: "Lax",
	});
	res.status(200).json({ message: "Logout successful" });
});

// DELETE endpoints follow the same pattern
const deleteProduct = async (req, res, collectionName) => {
	try {
		const result = await req[collectionName].deleteOne({
			id: parseInt(req.params.id),
		});
		if (result.deletedCount === 0) {
			res.status(404).json({
				message: "Delete operation failed",
				error: `${collectionName} not found`,
			});
			return;
		}
		res.json(result);
	} catch (err) {
		res.status(500).json({
			message: "Delete operation failed",
			error: err.message,
		});
	}
};

app.delete("/api/rest/v1/motorcycles/:id", async (req, res) => {
	await deleteProduct(req, res, "Motorcycle");
});

app.delete("/api/rest/v1/replacements/:id", async (req, res) => {
	await deleteProduct(req, res, "Replacement");
});

app.delete("/api/rest/v1/categories/:id", async (req, res) => {
	await deleteProduct(req, res, "Category");
});

// PUT endpoints follow the same pattern
const updateProduct = async (req, res, collectionName) => {
	try {
		const result = await req[collectionName].findOneAndUpdate(
			{ id: parseInt(req.params.id) },
			{ $set: req.body },
			{ returnDocument: "after" }
		);
		res.json(result.value);
	} catch (err) {
		res.status(500).json({
			message: `Update operation failed for ${collectionName}`,
			error: err.message,
		});
	}
};

app.put("/api/rest/v1/motorcycles/:id", async (req, res) => {
	await updateProduct(req, res, "Motorcycle");
});

app.put("/api/rest/v1/replacements/:id", async (req, res) => {
	await updateProduct(req, res, "Replacement");
});

app.put("/api/rest/v1/categories/:id", async (req, res) => {
	await updateProduct(req, res, "Category");
});

app.put("/api/rest/v1/orders/:id", async (req, res) => {
	await updateProduct(req, res, "Order");
});

const server = app.listen(port, () =>
	console.log(`Example app listening on port ${port}!`)
);

server.keepAliveTimeout = 120 * 1000;
server.headersTimeout = 120 * 1000;
