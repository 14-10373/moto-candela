import "./Order.scss";
import Item from "./Item/Item";
import PropTypes from "prop-types";
import { Button } from "../Button/Button";

const Order = ({ order, cancelOrder }) => (
	<>
		<div className="order-header">
			<div>Orden #{order._id.toUpperCase()}</div>
			<div>Fecha: {new Date(order.date).toLocaleDateString("es-VE")}</div>
			<div>Estado: <span className={`status-${order.status}`}>{order.status}</span></div>
			{order.status === "pendiente" && (
				<Button
					color="primary"
					onClick={() => cancelOrder(order.id)}
					disabled={order.status === "cancelada"}
				>
					Cancelar
				</Button>
			)}
		</div>
		<div className="order-list">
			{order.items.map((item) => (
				<Item
					key={item.id}
					item={item}
					exchangeRate={order.exchangeRate.$numberDecimal}
				/>
			))}
			<div className="order-summary__container">
				<div className="order-summary__titles">
					<div className="order-summary__titles-total">TOTAL</div>
					<div className="order-summary__titles-payment">
						METODO DE PAGO
					</div>
				</div>

				<div className="order-summary__dollar">
					<div className="order-summary__dollar-total">
						$ {order.amount.$numberDecimal}
					</div>
					<div className="order-summary__dollar-payment">
						{order.paymentMethod.toUpperCase()}
					</div>
				</div>

				<div className="order-summary__bs">
					<div className="order-summary__bs-total">
						Bs.{" "}
						{(
							order.amount.$numberDecimal *
							order.exchangeRate.$numberDecimal
						).toFixed(2)}
					</div>
					<div className="order-summary__bs-payment">&nbsp;</div>
				</div>
			</div>
		</div>
	</>
);

export default Order;

Order.propTypes = {
	order: PropTypes.shape({
		_id: PropTypes.string.isRequired,
		id: PropTypes.number.isRequired,
		date: PropTypes.string.isRequired,
		status: PropTypes.string,
		items: PropTypes.arrayOf(PropTypes.object),
		amount: PropTypes.shape({
			$numberDecimal: PropTypes.string.isRequired,
		}),
		exchangeRate: PropTypes.shape({
			$numberDecimal: PropTypes.string.isRequired,
		}),
		paymentMethod: PropTypes.string,
	}),
	cancelOrder: PropTypes.func,
};
