import "./Item.scss";
import PropTypes from "prop-types";
import { NavLink } from "react-router-dom";

const Item = ({ item, exchangeRate }) => {
	const tasaDeCambio = exchangeRate;

	const truncate = (text, maxLength) => {
		return text.length > maxLength
			? `${text.substring(0, maxLength)}...`
			: text;
	};

	const image =
		item.images.length >= 1 ? item.images[0] : "/motos/no-image.png";
	const description = item.description
		? item.description
		: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Numquam temporibus illum adipisci officia rerum explicabo porro commodi similique nobis asperiores quo, harum quas quae hic tempora ab aspernatur earum sapiente?";
	
	const price = item.price ? parseFloat(item.price.$numberDecimal).toFixed(2) : null;
    const dolares = price ? price : "N/A";
    const bs = price ? (price * tasaDeCambio).toFixed(2) : "N/A";

	return (
		<div className="order-item__container">
			<div className="order-item__content">
				<img className="order-item__content-image" src={image} />
				<div className="about">
					<p className="order-item__content-title">{item.name}</p>
					<p className="order-item__content-category">
						{item.category}
					</p>

					<p className="order-item__content-description">
						{truncate(description, 40)}
					</p>

					<div className="order-item__content-price">
						<p>$ {dolares}</p>
						<p>Bs. {bs}</p>
					</div>
				</div>
			</div>

			<div className="order-item__detail-link">
				<div className="product-card__action">
					<NavLink
						to={
							image.split("/")[1] === "motos"
								? `/motos/${item.id}`
								: `/repuestos/${item.id}`
						}
						className="product-card__action-button"
					>
						Ver más
					</NavLink>
				</div>
			</div>

			<div className="order-item__amount">
				<p>$ {dolares === "N/A" ? "N/A" : (dolares * item.count).toFixed(2)}</p>
				<p>Bs. {bs === "N/A" ? "N/A" : (bs * item.count).toFixed(2)}</p>
			</div>

			<div className="order-item__action">
				<div className="order-item__action-counter">{item.count}</div>
			</div>
		</div>
	);
};

export default Item;

Item.propTypes = {
	item: PropTypes.shape({
		id: PropTypes.number,
		name: PropTypes.string,
		category: PropTypes.string,
		description: PropTypes.string,
		images: PropTypes.arrayOf(PropTypes.string),
		characteristics: PropTypes.arrayOf(PropTypes.string),
		price: PropTypes.shape({
			$numberDecimal: PropTypes.string,
		}),
		count: PropTypes.number,
	}).isRequired,
	exchangeRate: PropTypes.string.isRequired,
};
