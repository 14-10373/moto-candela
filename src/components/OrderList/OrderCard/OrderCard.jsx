import { useState } from "react";
import { CircleButton } from "../../Button/Button";
import "./OrderCard.scss";
import PropTypes from "prop-types";

const OrderCard = ({ order, handleNavigation }) => {
	const [hover, setHover] = useState(false);
	const productImage = order?.items[0]?.images.length
		? order.items[0].images[0]
		: "";

	const date = new Date(order.date).toLocaleDateString("es-VE");

	return (
		<div className="card animate__animated animate__fadeIn animate__fast">
			{hover && (
				<div
					className="card__buttons"
					onMouseOver={() => setHover(true)}
					onMouseLeave={() => setHover(false)}
					onFocus={() => setHover(true)}
					onBlur={() => setHover(false)}
				>
					<CircleButton
						onClick={() => handleNavigation(order.id)}
						className="card__buttons__button card__buttons__button--details"
						iconName="IconArrowRight"
						color="primary"
					/>
				</div>
			)}
			<div
				className="overlay"
				onMouseOver={() => setHover(true)}
				onMouseLeave={() => setHover(false)}
				onFocus={() => setHover(true)}
				onBlur={() => setHover(false)}
			></div>
			<div className="card__text">
				<h3 className="card__text--title">{`Orden #${order._id.toUpperCase()}`}</h3>
				<p className="card__text--subtitle">{date}</p>
				<p className="card__text--subtitle">Estado: {order.status}</p>
			</div>
			<img
				className="card__image animate__animated animate__fadeIn animate__slow"
				src={productImage}
				alt="moto-image"
			/>
		</div>
	);
};

export default OrderCard;

OrderCard.propTypes = {
	order: PropTypes.object.isRequired,
	handleNavigation: PropTypes.func.isRequired,
};
