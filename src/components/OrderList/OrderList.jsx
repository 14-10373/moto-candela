import React, { useState, useEffect } from 'react'
import { SearchFilterBar } from '../SearchFilterBar/SearchFilterBar';
import { Pager } from '../Pager/Pager';
import OrderCard from './OrderCard/OrderCard';
import './OrderList.scss';

const OrderList = ({ orders, categories, searchBarLabel = 'Producto', criteria = '', handleNavigation }) => {
    const maxProductsPerPage = 5;
    const [orderList, setOrderList] = useState(orders)
    const [paginatedData, setPaginatedData] = useState(orders.slice(0, maxProductsPerPage));
    const sortByDate = (a, b) => {
            if (new Date(a.date) > new Date(b.date)) return -1;
            if (new Date(a.date) < new Date(b.date)) return 1;
            return 0;
        }

    useEffect(() => {
        setOrderList(orders.sort(sortByDate));
    }, [orders])

    useEffect(() => {
        setPaginatedData(orderList.slice(0, maxProductsPerPage));
    }, [orderList])

    const handleDataChange = (name) => {
        const filteredOrders = orders
            .filter(order => 
                order.items.some(item => item.name.toLowerCase().includes(name.toLowerCase()))
            ).sort(sortByDate);
        setOrderList(filteredOrders);
        setPaginatedData(filteredOrders.slice(0, maxProductsPerPage));
    }

    const handlePageChange = (data) => {
        setPaginatedData(data);
    }

    return (
        <div className='list'>
            <SearchFilterBar label={searchBarLabel} categories={categories} handleDataChange={handleDataChange} />
            <div className='list__orders'>
                {paginatedData.map(order => (
                    <OrderCard order={order} handleNavigation={handleNavigation} key={order.id} criteria={criteria || "category"} />
                ))}
            </div>
            <Pager data={orderList} handlePageChange={handlePageChange} maxProductsPerPage={maxProductsPerPage}/>
        </div>
    )
}

export default OrderList;