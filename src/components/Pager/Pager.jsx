import React, { useState } from 'react';
import { IconChevronLeft, IconChevronRight } from '@tabler/icons-react';
import PropTypes from 'prop-types'
import './Pager.scss';
export const Pager = ({ data = [], handlePageChange, maxProductsPerPage=10 }) => {

    const [currentPage, setCurrentPage] = useState(1);
    const totalPages = Math.ceil(data.length / maxProductsPerPage) || 1;

    const nextPage = (currentPage) => {
        if (currentPage < totalPages) {
            handlePageChange(getDataPerPage(currentPage + 1));
            setCurrentPage(currentPage + 1);
        }
    }

    const previousPage = (currentPage) => {
        if (currentPage > 1) {
            handlePageChange(getDataPerPage(currentPage - 1));
            setCurrentPage(currentPage - 1);
        }
    }

    const getDataPerPage = (page) => {
        const start = (page - 1) * maxProductsPerPage;
        const end = start + maxProductsPerPage;
        return data.slice(start, end);
    };

    return (
        <div className='pager'>
            <button disabled={currentPage === 1} onClick={() => previousPage(currentPage)} className='pager__arrow pager__arrow--left'><IconChevronLeft />Anterior</button>
            <span>{currentPage} de {totalPages}</span>
            <button disabled={currentPage === totalPages} onClick={() => nextPage(currentPage)} className='pager__arrow pager__arrow--right'>Siguiente<IconChevronRight /></button>
        </div>
    )
}

Pager.propTypes = {
    data: PropTypes.array,
    handlePageChange: PropTypes.func
}