import { useContext, useEffect, useState } from "react";
import "./User.scss";
import { Button } from "../Button/Button";
import UserContext from "../../providers/UserContext";
import { logoutUser } from "../../services/users";
import { useNavigate } from "react-router-dom";
import { getOrders } from "../../services/orders";
import OrderList from "../OrderList/OrderList";

export default () => {
	const { user, loggedIn, setUser, setLoggedIn } = useContext(UserContext);
	const [orders, setOrders] = useState([]);

	const navigate = useNavigate();

	const logout = async () => {
		await logoutUser();
		setUser(null);
		setLoggedIn(false);
		navigate("/");
	};

	const fetchOrders = async () => {
		if (!user) return;
		const orders = await getOrders();
		setOrders(orders.filter((order) => order.user._id === user._id));
	};

	useEffect(() => {
		if (!loggedIn) {
			navigate("/");
		}
	}, [loggedIn, navigate]);

	useEffect(() => {
		fetchOrders();
	}, []);

	return (
		<div className='user-container'>
			<div className="user-content">
				<h2 className="user-content__title">Información de usuario</h2>
				<div className="user-content__description">
					<p><strong>Email:</strong> {user ? user.email : "Cargando..."}</p>
					<h3>Ordenes:</h3>
					{!user ? "Cargando..."
					:
						<OrderList orders={orders} handleNavigation={(id) => navigate(`/orders/${id}`)} />
					}
				</div>

				<Button
					color="primary"
					onClick={logout}
					disabled={!loggedIn}
				>
					Cerrar sesión
				</Button>
			</div>
		</div>
	);
};
