import { useContext, useState, useEffect } from "react";
import "./Cart.scss";
import { Button } from "../Button/Button";
import CartItem from "./CartItem/CartItem";
import CartSummary from "./CartSummary/CartSummary";
import CartContext from "../../providers/CartContext";
import efectivoImg from "../../assets/icons/efectivo.png";
import pagoMovilImg from "../../assets/icons/pago-movil.png";
import { postOrder } from "../../services/orders";
import { useNavigate } from "react-router-dom";
import UserContext from "../../providers/UserContext";
import PropTypes from "prop-types";

export default () => {
	const { cart, clearCart, tasaDeCambio, IVA } = useContext(CartContext);
	const { user, loggedIn } = useContext(UserContext);
	const [subtotal, setSubtotal] = useState(getSubtotal(cart));
	const [resumen, setResumen] = useState(
		getResumen(subtotal, tasaDeCambio, IVA)
	);

	const [showMetodoPago, setShowMetodoPago] = useState(false);
	const [metodoDePago, setMetodoDePago] = useState("");

	const [procesando, setProcesando] = useState(false);
	const [ordenFinalizada, setOrdenFinalizada] = useState(false);

	useEffect(() => {
		setSubtotal(getSubtotal(cart));
	}, [cart]);

	useEffect(() => {
		setResumen(getResumen(subtotal, tasaDeCambio, IVA));
	}, [IVA, subtotal, tasaDeCambio]);

	const navigate = useNavigate();

	const proceedToPayment = () => {
		if (loggedIn) {
			setShowMetodoPago(true);
		} else {
			navigate("/login");
		}
	};

	const placeOrder = async () => {
		setProcesando(true);

		const order = {
			date: new Date(),
			user,
			items: cart,
			amount: {
				$numberDecimal: resumen.totalDolares.toFixed(2),
			},
			paymentMethod: metodoDePago,
			exchangeRate: {
				$numberDecimal: tasaDeCambio.toFixed(2),
			},
			status: "pendiente",
		};

		const res = await postOrder(order);

		console.log(res);

		setProcesando(false);
		setOrdenFinalizada(true);

		clearCart();
	};

	return (
		<div className={`cart-container ${procesando ? "processing" : ""}`}>
			{ordenFinalizada ? (
				<div className="orden-finalizada">
					<h3>Orden finalizada</h3>
					<p>
						Gracias por su compra, en breve nos pondremos en
						contacto con usted para coordinar el pago y la entrega.
					</p>
					<Button color="primary" onClick={() => navigate("/")}>
						Volver al inicio
					</Button>
				</div>
			) : (
				<div className="cart-list">
					{showMetodoPago ? (
						<RenderMetodoDePago
							metodoDePago={metodoDePago}
							setMetodoPago={setMetodoDePago}
						/>
					) : (
						cart.map((item) => (
							<CartItem item={item} key={item.id} />
						))
					)}

					<CartSummary resumen={resumen} />

					{!showMetodoPago ? (
						<Button
							color="primary"
							onClick={cart.length ? proceedToPayment : null}
							disabled={cart.length === 0}
						>
							Método de pago
						</Button>
					) : (
						<div className="button-cart-navigation">
							<Button
								color="primary"
								onClick={() => setShowMetodoPago(false)}
							>
								Volver
							</Button>
							<Button
								disabled={metodoDePago === ""}
								color="primary"
								onClick={() => placeOrder()}
							>
								Finalizar orden
							</Button>
						</div>
					)}
				</div>
			)}
		</div>
	);
};

function RenderMetodoDePago({ metodoDePago, setMetodoPago }) {
	return (
		<div className="metodo-de-pago">
			<h3>Seleccione el método de pago</h3>
			<div className="metodo-de-pago__buttons">
				<div
					className={`metodo-de-pago__buttons__button${
						metodoDePago === "pago movil" ? " button-active" : ""
					}`}
					color="primary"
					onClick={() => {
						setMetodoPago("pago movil");
					}}
					onKeyDown={(event) => {
						if (event.key === "Enter" || event.key === " ") {
							setMetodoPago("pago movil");
						}
					}}
					tabIndex={0}
				>
					<div className="metodo-de-pago__buttons__button__label">
						Pago móvil
					</div>
					<img
						className="metodo-de-pago__buttons__button__icon"
						src={pagoMovilImg}
						alt="pago movil"
					/>
				</div>
				<div
					className={`metodo-de-pago__buttons__button${
						metodoDePago === "efectivo" ? " button-active" : ""
					}`}
					color="primary"
					onClick={() => {
						setMetodoPago("efectivo");
					}}
					onKeyDown={(event) => {
						if (event.key === "Enter" || event.key === " ") {
							setMetodoPago("efectivo");
						}
					}}
					tabIndex={0}
				>
					<div className="metodo-de-pago__buttons__button__label">
						Efectivo
					</div>
					<img
						className="metodo-de-pago__buttons__button__icon"
						src={efectivoImg}
						alt="efectivo"
					/>
				</div>
			</div>
		</div>
	);
}

function getSubtotal(cart) {
	return cart.reduce((acc, item) => {
		if (item.price)
			return acc + parseFloat(item.price.$numberDecimal) * item.count;
		return acc;
	}, 0);
}

function getResumen(subtotal, tasaDeCambio, IVA) {
	const subDolares = +subtotal.toFixed(2);
	const subBs = +(subDolares * tasaDeCambio).toFixed(2);

	const ivaDolares = +(subDolares * IVA).toFixed(2);
	const ivaBs = +(subBs * IVA).toFixed(2);

	const totalDolares = +(subDolares + ivaDolares).toFixed(2);
	const totalBs = +(subBs + ivaBs).toFixed(2);

	return {
		IVA,
		subDolares,
		subBs,
		ivaDolares,
		ivaBs,
		totalDolares,
		totalBs,
	};
}

RenderMetodoDePago.propTypes = {
	metodoDePago: PropTypes.string.isRequired,
	setMetodoPago: PropTypes.func.isRequired,
};
