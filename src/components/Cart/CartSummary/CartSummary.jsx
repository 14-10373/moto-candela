import { useContext } from "react";
import CartContext from "../../../providers/CartContext";
import "./CartSummary.scss";
import PropTypes from "prop-types";

const CartSummary = ({ resumen }) => {
	const {
		IVA,
		subDolares,
		subBs,
		ivaDolares,
		ivaBs,
		totalDolares,
		totalBs,
	} = resumen;

	return (
		<div className="cart-summary__container">
			<div className="cart-summary__titles">
				<div className="cart-summary__titles-subtotal">Subtotal</div>
				<div className="cart-summary__titles-iva">
					+ I.V.A ({IVA * 100}%)
				</div>
				<div className="cart-summary__titles-total">TOTAL</div>
			</div>

			<div className="cart-summary__dollar">
				<div className="cart-summary__dollar-subtotal">
					$ {subDolares}
				</div>
				<div className="cart-summary__dollar-iva">$ {ivaDolares}</div>
				<div className="cart-summary__dollar-total">
					$ {totalDolares}
				</div>
			</div>

			<div className="cart-summary__bs">
				<div className="cart-summary__bs-subtotal">Bs. {subBs}</div>
				<div className="cart-summary__bs-iva">Bs. {ivaBs}</div>
				<div className="cart-summary__bs-total">Bs. {totalBs}</div>
			</div>
		</div>
	);
};

export default CartSummary;

CartSummary.propTypes = {
	resumen: PropTypes.shape({
		IVA: PropTypes.number.isRequired,
		subDolares: PropTypes.number.isRequired,
		subBs: PropTypes.number.isRequired,
		ivaDolares: PropTypes.number.isRequired,
		ivaBs: PropTypes.number.isRequired,
		totalDolares: PropTypes.number.isRequired,
		totalBs: PropTypes.number.isRequired,
	}),
};
