import { NavLink } from "react-router-dom";
import Navbar from "../Navbar/Navbar";
import Topbar from "../Topbar/Topbar";
import Logo from '/logo.svg';
import "./Header.scss";
import PropTypes from "prop-types";

const Header = (props) => {
  return (
    <header className="header__fixed">
      <Navbar links={props.navigation}
        active={props.selected} handler={props.handleSelection}>
        <LogoNavLink {...props} />
      </Navbar>
      <Topbar links={props.navigation}
        active={props.selected} handler={props.handleSelection}>
        <LogoNavLink {...props} />
      </Topbar>
    </header>
  );
}

const LogoNavLink = (props) => {
  return (
    <NavLink to="/">
      <img
        onClick={() => props.handleSelection("")}
        onKeyDown={(event) => {
          if (event.key === 'Enter' || event.key === ' ') {
            props.handleSelection("");
          }
        }}
        tabIndex={0}
        className="header__logo" src={Logo}
      />
    </NavLink>
  );
}

export default Header;

Header.propTypes = {
  navigation: PropTypes.array.isRequired,
  selected: PropTypes.string.isRequired,
  handleSelection: PropTypes.func.isRequired,
};

LogoNavLink.propTypes = {
  handleSelection: PropTypes.func.isRequired,
};