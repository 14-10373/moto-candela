import { NavLink } from "react-router-dom";
import * as Icons from "@tabler/icons-react";
import CartIcon from "../Cart/CartIcon/CartIcon";
import PropTypes from "prop-types";

const HeaderLinkList = ({ links, active, handler }) => {
    const ICON_MAP = {
        "Carrito": CartIcon,
        "Ingresar": Icons.IconLogin,
        "Usuario": Icons.IconUser,
    }
	return (
		<ul>
			{links.map((link) => {
				const linkKey = `nav-link-${link.name}`;
				const IconComponent = ICON_MAP[link.name];

				return (
					<li
						key={linkKey}
						id={linkKey}
						className={`${active === linkKey ? "active" : ""}`}
						onClick={(event) => handler(event.target.id)}
						onKeyDown={(event) => {
							if (event.key === 'Enter' || event.key === ' ') {
								handler(event.target.id);
							}
						}}
						tabIndex="0" // Make the li focusable
					>
						<NavLink to={link.path}>
							{IconComponent ?
								<IconComponent size={24} color="#FFF" />
                                : link.name
							}
						</NavLink>
					</li>
				);
			})}
		</ul>
	);
};

export default HeaderLinkList;

HeaderLinkList.propTypes = {
	links: PropTypes.array.isRequired,
	active: PropTypes.string.isRequired,
	handler: PropTypes.func.isRequired,
};