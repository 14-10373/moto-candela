import { InputAdornment, TextField } from "@mui/material";
import {
	IconLock,
	IconUser,
} from "@tabler/icons-react";
import { useEffect, useState, useContext } from "react";
import "./Register.scss";
import { Button } from "../../components/Button/Button";
import logoSmall from "./../../assets/img/logo-small.svg";
import { getUsers, registerUser, loginUser } from "../../services/users";
import { useNavigate } from "react-router-dom";
import UserContext from "../../providers/UserContext";
import validator from "validator";
import PropTypes from "prop-types";

export default () => {
	const { setLoggedIn, setUser } = useContext(UserContext);

	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [passwordConfirm, setPasswordConfirm] = useState("");

	const [validEmail, setValidEmail] = useState(false);
	const [validPassword, setValidPassword] = useState(false);
	const [passwordsMatch, setPasswordsMatch] = useState(false);

	const [registerError, setRegisterError] = useState("");

	const navigate = useNavigate();

	const handleChangeEmail = (event) => {
		setEmail(event.target.value);
	};

	const handleChangePassword = (event) => {
		setPassword(event.target.value);
	};

	const handleChangePasswordConfirm = (event) => {
		setPasswordConfirm(event.target.value);
	};

	const handleSubmit = async () => {
		// check if email is already registered
		const users = await getUsers();

		const user = users.find((user) => user.email === email);

		if (user) {
			setRegisterError("El correo electrónico ya está registrado.");
			return;
		}

		const newUser = {
			email,
			password,
		};

		const res = await registerUser(newUser);

		if (res.error) {
			setRegisterError("Error al registrar el usuario.");
			return;
		}

		const loginRes = await loginUser(newUser);

		if (loginRes.error) {
			setRegisterError(loginRes.error);
			return;
		}

		setLoggedIn(true);
		setUser(loginRes);
		navigate("/");
	};

	const handleKeyUp = async (event) => {
		if (validInfo() && event.key === "Enter") {
			await handleSubmit();
		}
	};

	useEffect(() => {
		if (email !== "") {
			setValidEmail(validator.isEmail(email));
		} else {
			setValidEmail(false);
		}
	}, [email]);

	useEffect(() => {
		if (password !== "") {
            const re = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{8,}$/;
			setValidPassword(re.test(password));
		} else {
			setValidPassword(false);
		}
	}, [password]);

	useEffect(() => {
		if (password === "" || passwordConfirm === "") {
			setPasswordsMatch(false);
		} else {
			setPasswordsMatch(password === passwordConfirm);
		}
	}, [password, passwordConfirm]);

	const validInfo = () => {
		return validEmail && validPassword && passwordsMatch;
	};

	return <RegisterForm
		registerError={registerError}
		email={email}
		password={password}
		passwordConfirm={passwordConfirm}
		validEmail={validEmail}
		validPassword={validPassword}
		passwordsMatch={passwordsMatch}
		handleChangeEmail={handleChangeEmail}
		handleChangePassword={handleChangePassword}
		handleChangePasswordConfirm={handleChangePasswordConfirm}
		handleSubmit={handleSubmit}
		handleKeyUp={handleKeyUp}
		validInfo={validInfo}
	/>
};


function RegisterForm({
	registerError,
	email,
	password,
	passwordConfirm,
	validEmail,
	validPassword,
	passwordsMatch,
	handleChangeEmail,
	handleChangePassword,
	handleChangePasswordConfirm,
	handleSubmit,
	handleKeyUp,
	validInfo,
}) {
	const colorHints = {
		red: 'red',
		green: 'green',
		gray: '#49454F'
	}
	const hintStatus = {}

	if (email.length === 0) {
		hintStatus.email = colorHints.gray
	} else if (validEmail) {
		hintStatus.email = colorHints.green
	} else {
		hintStatus.email = colorHints.red
	}

	if (password.length === 0) {
		hintStatus.password = colorHints.gray
	} else if (validPassword) {
		hintStatus.password = colorHints.green
	} else {
		hintStatus.password = colorHints.red
	}

	if (passwordConfirm.length === 0) {
		hintStatus.passwordConfirm = colorHints.gray
	} else if (passwordsMatch) {
		hintStatus.passwordConfirm = colorHints.green
	} else {
		hintStatus.passwordConfirm = colorHints.red
	}


	return (
	<div className="login">
		<img
			className="login__logo"
			width={100}
			src={logoSmall}
			alt="Moto Candela Logo Pequeño"
		/>
		<div className="login__fields">
			{registerError && (
				<div
					style={{
						color: "red",
						position: "absolute",
						top: "110px",
					}}
				>
					{registerError}
				</div>
			)}
			{/* <Tooltip
				open={email.length && !validEmail}
				title="Introduce una dirección de correo electrónico válida."
				placement="bottom"
			> */}
				<TextField
					placeholder="Correo electrónico"
					variant="outlined"
					color="primary"
					size="small"
					value={email}
					onChange={handleChangeEmail}
					type="email"
					autoComplete="email"
					sx={{
						width: "90%",
						backgroundColor: "white",
						borderRadius: 5,
						"& .MuiOutlinedInput-root": {
							"& fieldset": {
								borderRadius: "50px",
							},
						},
					}}
					InputProps={{
						endAdornment: (
							<InputAdornment position="end">
								<IconUser
									size={20}
									color={hintStatus.email}
								/>
							</InputAdornment>
						),
					}}
					inputProps={{
						title: "Introduce una dirección de correo electrónico válida.",
					}}
					onKeyUp={handleKeyUp}
				/>
			{/* </Tooltip> */}

			{/* <Tooltip
				open={password.length && !validPassword}
				title="La contraseña debe tener al menos 8 caracteres de longitud y contener al menos una letra minúscula, una letra mayúscula y un dígito."
				placement="bottom"
			> */}
				<TextField
					placeholder="Contraseña"
					variant="outlined"
					color="primary"
					size="small"
					value={password}
					onChange={handleChangePassword}
					type="password"
					autoComplete="new-password"
					sx={{
						width: "90%",
						backgroundColor: "white",
						borderRadius: 5,
						"& .MuiOutlinedInput-root": {
							"& fieldset": {
								borderRadius: "50px",
							},
						},
					}}
					InputProps={{
						endAdornment: (
							<InputAdornment position="end">
								<IconLock
									size={20}
									color={hintStatus.password}
								/>
							</InputAdornment>
						),
					}}
					inputProps={{
						title: "La contraseña debe tener al menos 8 caracteres de longitud y contener al menos una letra minúscula, una letra mayúscula y un dígito.",
					}}
					onKeyUp={handleKeyUp}
				/>
			{/* </Tooltip> */}
			{/* <Tooltip
				open={passwordConfirm.length && !passwordsMatch}
				title="La confirmación de la contraseña debe coincidir con la contraseña ingresada."
				placement="bottom"
			> */}
				<TextField
					placeholder="Confirmar contraseña"
					variant="outlined"
					color="primary"
					size="small"
					value={passwordConfirm}
					onChange={handleChangePasswordConfirm}
					type="password"
					autoComplete="new-password"
					sx={{
						width: "90%",
						backgroundColor: "white",
						borderRadius: 5,
						"& .MuiOutlinedInput-root": {
							"& fieldset": {
								borderRadius: "50px",
							},
						},
					}}
					InputProps={{
						endAdornment: (
							<InputAdornment position="end">
								<IconLock
									size={20}
									color={hintStatus.passwordConfirm}
								/>
							</InputAdornment>
						),
					}}
					inputProps={{
						title: "La confirmación de la contraseña debe coincidir con la contraseña ingresada.",
					}}
					onKeyUp={handleKeyUp}
				/>
			{/* </Tooltip> */}
			<div className="login__fields__forgot">
				Ya tienes cuenta?{" "}
				<a className="register" href="/login">
					Inicia sesion
				</a>
			</div>
		</div>
		<div className="login__buttons">
			<Button
				color="secondary"
				onClick={validInfo() ? handleSubmit : null}
				disabled={!validInfo()}
			>
				Registrarse
			</Button>
			{/* <Button color="secondary" onClick={handleGoogleSubmit} customStyle={{ backgroundColor: '#E23A2E' }}>
				<img src={googleLogo} width={24} /> Registrarse con Google
			</Button> */}
		</div>
	</div>
)}

RegisterForm.propTypes = {
	registerError: PropTypes.string.isRequired,
	email: PropTypes.string.isRequired,
	password: PropTypes.string.isRequired,
	passwordConfirm: PropTypes.string.isRequired,
	validEmail: PropTypes.bool.isRequired,
	validPassword: PropTypes.bool.isRequired,
	passwordsMatch: PropTypes.bool.isRequired,
	handleChangeEmail: PropTypes.func.isRequired,
	handleChangePassword: PropTypes.func.isRequired,
	handleChangePasswordConfirm: PropTypes.func.isRequired,
	handleSubmit: PropTypes.func.isRequired,
	handleKeyUp: PropTypes.func.isRequired,
	validInfo: PropTypes.func.isRequired,
};