const API_URL = `${import.meta.env.VITE_API_URL}/api/rest/v1`;

export async function getOrders() {
    const response = await fetch(`${API_URL}/orders`);
    const data = await response.json();
    return data;
}

export async function postOrder(order) {
    try {
        const response = await fetch(`${API_URL}/orders`, {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(order)
        });
        const data = await response.json();
        return data;
    } catch (err) {
        console.log(err);
    }
}

export async function getOrderById(id) {
    const response = await fetch(`${API_URL}/orders/${id}`);
    const data = await response.json();
    return data;
}

export async function updateOrder(id, changes) {
    try {
        const response = await fetch(`${API_URL}/orders/${id}`, {
            method: 'PUT',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(changes)
        });
        const data = await response.json();
        return data;
    } catch (err) {
        console.log(err);
    }
}