const API_URL = `${import.meta.env.VITE_API_URL}/api/rest/v1`;

export async function getUsers() {
    const response = await fetch(`${API_URL}/users`);
    const data = await response.json();
    return data;
}

export async function registerUser(user) {
    try {
        const response = await fetch(`${API_URL}/users`, {
            method: 'POST',
            credentials: 'include',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(user)
        });
        const data = await response.json();
        return data;
    } catch (err) {
        console.log(err);
    }
}

export async function loginUser(user) {
    try {
        const response = await fetch(`${API_URL}/users/login`, {
            method: 'POST',
            credentials: 'include',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(user)
        });
        const data = await response.json();
        return data;
    } catch (err) {
        console.log(err);
    }
}

export async function getAuthenticatedUser() {
    try {
        const response = await fetch(`${API_URL}/authenticateUser`, {
            credentials: 'include',
            headers: { 'Content-Type': 'application/json' }
        });
        const data = await response.json();
        return data;
    } catch (err) {
        console.log(err);
    }
}

export async function logoutUser() {
    try {
        const response = await fetch(`${API_URL}/users/logout`, {
            method: 'POST',
            credentials: 'include',
            headers: { 'Content-Type': 'application/json' }
        });
        const data = await response.json();
        return data;
    } catch (err) {
        console.log(err);
    }
}