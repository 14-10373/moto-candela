import { useState, useEffect, useCallback } from "react";
import { useParams } from 'react-router-dom';
import Banner from "../components/Banner/Banner";
import BannerImage from "../assets/banners/banner.png";
import Order from "../components/Order/Order";
import { getOrderById, updateOrder } from "../services/orders";

export default () => {
    const { id } = useParams();
    const [order, setOrder] = useState(null);

    const fetchOrder = useCallback(async () => {
        const orderData = await getOrderById(id);
        setOrder(orderData);
    }, [id]);

    useEffect(() => {
        fetchOrder();
    }, [fetchOrder])

    const cancelOrder = async (id) => {
        const updatedOrder = await updateOrder(id, { status: 'cancelada' });
        setOrder(updatedOrder);
    }

    return (
        <>
            <Banner image={BannerImage} title="DETALLE DE ORDEN" />
            {order && <Order order={order} cancelOrder={cancelOrder} />}
        </>
    );
}