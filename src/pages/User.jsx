import Banner from "../components/Banner/Banner";
import BannerImage from "../assets/banners/banner.png";
import User from "../components/User/User";

export default () => {
    return (
        <>
            <Banner image={BannerImage} title="USUARIO" />
            <User />
        </>
    );
}