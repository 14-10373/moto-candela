import React, { useEffect } from "react";
import UserContext from "./UserContext";
import PropTypes from "prop-types";
import { getAuthenticatedUser } from "../services/users";

const UserProvider = ({ children }) => {
	const [user, setUser] = React.useState(null);
	const [loggedIn, setLoggedIn] = React.useState(false);
	const [loading, setLoading] = React.useState(true);

	const authenticate = async () => {
		const authenticatedUser = await getAuthenticatedUser();
		setLoggedIn(authenticatedUser ? true : false);
		setUser(authenticatedUser);
		setLoading(false);
	};

	useEffect(() => {
		authenticate();
	}, []);
	
	return (
		<UserContext.Provider
			value={{
				user,
				setUser,
				loggedIn,
				setLoggedIn,
			}}
		>
			{!loading && children}
		</UserContext.Provider>
	);
};

export default UserProvider;

UserProvider.propTypes = {
	children: PropTypes.node.isRequired,
};
