import React from "react";
import CartContext from "./CartContext";
import PropTypes from "prop-types";

const CartProvider = ({ children }) => {
	const tasaDeCambio = 35;
	const IVA = 0.16;

	const [cart, setCart] = React.useState(
		localStorage.getItem("cart")
			? JSON.parse(localStorage.getItem("cart"))
			: []
	);

	React.useEffect(() => {
		localStorage.setItem("cart", JSON.stringify(cart));
	}, [cart]);

	const addToCart = React.useCallback(
		(item) => {
			const itemIndex = cart.findIndex(
				(cartItem) =>
					cartItem.id === item.id && cartItem._id === item._id
			);
			if (itemIndex !== -1) {
				// Item is already in the cart, increment the count
				const newCart = [...cart];
				newCart[itemIndex].count += 1;
				setCart(newCart);
			} else {
				// Item is not in the cart, add it with a count of 1
				setCart([...cart, { ...item, count: 1 }]);
			}
		},
		[cart, setCart]
	);

	const removeFromCart = React.useCallback(
		(item) => {
			const itemIndex = cart.findIndex(
				(cartItem) =>
					cartItem.id === item.id && cartItem._id === item._id
			);
			if (itemIndex !== -1) {
				const newCart = [...cart];
				if (newCart[itemIndex].count > 1) {
					// If item count is more than 1, decrease the count
					newCart[itemIndex].count -= 1;
				} else {
					// If item count is 1, remove the item from the cart
					newCart.splice(itemIndex, 1);
				}
				setCart(newCart);
			}
		},
		[cart, setCart]
	);

	const clearCart = React.useCallback(() => {
		setCart([]);
	}, [setCart]);

	const value = React.useMemo(
		() => ({
			tasaDeCambio,
			IVA,
			cart,
			addToCart,
			removeFromCart,
			clearCart,
		}),
		[tasaDeCambio, IVA, cart, addToCart, removeFromCart, clearCart]
	);

	return (
		<CartContext.Provider value={value}>{children}</CartContext.Provider>
	);
};

export default CartProvider;

CartProvider.propTypes = {
	children: PropTypes.node.isRequired,
};
